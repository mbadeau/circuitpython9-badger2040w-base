import board
import terminalio
import displayio
import vectorio
import time
from digitalio import DigitalInOut, Direction, Pull
from adafruit_display_text import label

# CircuitPython 9.0
# https://circuitpython.org/board/pimoroni_badger2040w/

"""
An example of how to show text on the Badger 2040's
screen using the built-in DisplayIO object.
"""

display = board.DISPLAY

BUTTON_A = DigitalInOut(board.SW_A)
BUTTON_B = DigitalInOut(board.SW_B)
BUTTON_C = DigitalInOut(board.SW_C)
BUTTON_UP = DigitalInOut(board.SW_UP)
BUTTON_DOWN = DigitalInOut(board.SW_DOWN)
BUTTON_A.direction = Direction.INPUT
BUTTON_B.direction = Direction.INPUT
BUTTON_C.direction = Direction.INPUT
BUTTON_UP.direction = Direction.INPUT
BUTTON_DOWN.direction = Direction.INPUT
BUTTON_A.pull = Pull.DOWN
BUTTON_B.pull = Pull.DOWN
BUTTON_C.pull = Pull.DOWN
BUTTON_UP.pull = Pull.DOWN
BUTTON_DOWN.pull = Pull.DOWN

LED = DigitalInOut(board.USER_LED)
LED.direction = Direction.OUTPUT


# Set text, font, and color
title = "HELLO WORLD"
subtitle = "From CircuitPython"
font = terminalio.FONT
color = 0x000000

# Set the palette for the background color
palette = displayio.Palette(1)
palette[0] = 0xFFFFFF

# Add a background rectangle
rectangle = vectorio.Rectangle(pixel_shader=palette, width=display.width + 1, height=display.height, x=0, y=0)

# Create the title and subtitle labels
title_label = label.Label(font, text=title, color=color, scale=4)
subtitle_label = label.Label(font, text=subtitle, color=color, scale=2)

# Set the label locations
title_label.x = 20
title_label.y = 45

subtitle_label.x = 40
subtitle_label.y = 90

# Create the display group and append objects to it
group = displayio.Group()
group.append(rectangle)
group.append(title_label)
group.append(subtitle_label)

# Show the group and refresh the screen to see the result
display.root_group = group
print(display.time_to_refresh)
time.sleep(display.time_to_refresh + 15)
display.refresh()

# Loop forever so you can enjoy your message
while True:
    if BUTTON_A.value:
        LED.value = True
    elif BUTTON_B.value:
        LED.value = False
