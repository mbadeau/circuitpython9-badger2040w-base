# Badger2040w example

This is a Circuitpython 9.0 version of the [example code](https://github.com/pimoroni/pico-circuitpython-examples/blob/main/badger2040/displayio_example.py) for the badger2040w.